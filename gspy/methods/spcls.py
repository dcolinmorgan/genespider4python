import numpy as np
from scipy import sparse
from scipy.sparse import linalg
from numpy.core.umath_tests import inner1d
import pandas as pd
# from cvxopt import cholmod, spmatrix


class spcls():

    def __init__(self, Ainit, Y, P, R, verbose=False, alpha=0.01, beta=0.5, prec=1e-6, max_steps=100, max_bt_steps=100):
        self.v = verbose
        self.alpha = alpha
        self.beta = beta
        self.prec = prec
        self.max_steps = max_steps
        self.max_bt_steps = max_bt_steps
        self.n = Ainit.shape[0]
        self.m = Ainit.shape[1]
        self.d = self.n * self.m
        if sparse.issparse(Ainit):
            self.xhat = Ainit.asformat("lil").reshape((1, np.prod(Ainit.shape))).asformat("csc")
        else:
            self.xhat = sparse.csc_matrix(np.reshape(Ainit, self.d, order="F"))

        self.Y = Y.copy()
        self.P = P.copy()
        if sparse.issparse(R):
            self.R = R
        else:
            self.R = sparse.dia_matrix(R)

        self.state = 'Unsolved'
        tol = np.finfo(float).eps
        self.k = np.sum((np.abs(Ainit) > tol).astype(int))
        if self.k == 0:
            self.state = "Solved"
            # self.results = Ainit

            if self.v:
                print("Zero networks are trivial. Completed with status = " + self.state)

            return

        self.z = np.zeros(self.k)  # optimization variable

        self.independent = True
        if self.Y.shape[0] > self.Y.shape[1]:
            self.independent = False

        self.affine()
        self.presets()
        self.hessian()

    def affine(self):
        F = sparse.lil_matrix((self.d, self.k), dtype=float)
        tol = np.finfo(float).eps
        nonzero = np.abs(self.xhat) > tol
        # ix = 0
        it = nonzero.nonzero()[1]
        for i in range(len(it)):
            F[it[i], i] = 1.0

        self.F = F.tocsr()

    def x(self, z):
        return sparse.csc_matrix(self.F.dot(z)) + self.xhat

    def A(self, x):
        return x.tolil().reshape((self.n, self.m)).tocsr().transpose()

    def g(self, A):
        return self.R.transpose().dot(A.dot(self.Y) + self.P)

    def f(self, A):
        gx = self.g(A)
        return np.sum(inner1d(gx, gx))

    def presets(self):
        self.RRt = self.R.dot(self.R.transpose())
        self.YYt = self.Y.dot(self.Y.transpose())
        self.gterm2 = 2 * self.RRt.dot(self.P.dot(self.Y.transpose()))

    def grad(self, A):
        J = 2 * self.RRt.dot(A.dot(self.YYt)) + self.gterm2
        return np.reshape(J, self.d, order="F")

    def hessian(self):
        '''Hessian is constant'''

        H = self.F.transpose().dot(sparse.kron(self.YYt, 2 * self.RRt).dot(self.F))
        
        if self.independent:
            H_L = sparse.csr_matrix(np.linalg.cholesky(H.todense()))
            # Comment: H_L = tp(H_L)  # A = H_L * tp(H_L)
            H_out = H_L
        else:  # Should be positive definite
            H_out = sparse.csr_matrix(np.linalg.pinv(H.todense()))  # pseudoinverse

        self.H = H_out

    def line_search(self, znt, g, fx):

        # z = np.zeros(self.F.shape[1])
        t = 1 / self.beta
        lhs = np.infty
        rhs = np.infty
        btx = 0

        while True:
            t = self.beta * t

            lhs = self.f(self.A(sparse.csc_matrix(self.xhat + self.F.dot(self.z + t * znt))))

            rhs = fx + self.alpha * t * g.dot(znt)

            if (lhs < rhs).all():
                self.z = self.z + t * znt
                return btx

            btx = btx + 1
            if btx > self.max_bt_steps:
                self.state = "Failed"
                raise StopIteration('Short-circuited line search')

    def update(self, i):

        A = self.A(self.x(self.z))
        fx = self.f(A)
        g = self.F.transpose().dot(self.grad(A))
        if self.independent:
            znt = -linalg.lsqr(self.H.transpose(), linalg.lsqr(self.H, g)[0])[0]
        else:
            znt = -self.H.dot(g)

        gap = -0.5 * g.dot(znt)

        btx = self.line_search(znt, g, fx)

        if self.v:
            self.verbose(i=i, fx=fx, gap=gap, btx=btx)

        # stopping
        if gap < self.prec:
            self.state = "Solved"
            if self.v:
                self.status()

            return

    def result():
        return self.A(self.x(self.z))

    def verbose(self, i=np.nan,fx=np.nan,gap=np.nan,btx=np.nan):
        frob = np.linalg.norm((self.A(self.x(self.z)).dot(self.Y) + self.P).transpose().dot(self.R.todense()), "fro")

        if np.isnan(i):
            run_options = " Optimization settings\n{} free variables, precision={}, alpha={}, beta={}".format(self.k, self.prec, self.alpha, self.beta)
            header = "{:>14}{:>15}{:>15}{:>14}{:>18}".format("gradient step", "value", "gap", "line steps", "|| (AY+P)\'*R ||")
            divider= "-" * len(header)
            print(run_options)
            print()
            print(header)
            print(divider)
        else:
            optimization_state = " {:13d}{:15.4g}{:15.4g}{:14}{:18.4g}".format(i, fx, gap, btx, frob)
            print(optimization_state)

    def status(self, v=None):
        if v is None:
            v = self.v

        if v:
            print("Current state is: {}".format(self.state))
        else:
            return self.state

    def cls(self):

        if self.v:
            self.verbose()

        for i in range(self.max_steps):
            self.update(i)
            if self.state == "Solved":
                break


def main():

    testA = (np.abs(net.A) > 0).astype(float)
    cls = spcls(testA, data.Y, data.P, np.eye(testA.shape[0]))

# %run spcls.py
# testA = (np.abs(net.A) > 0).astype(float)
# cls = spcls(testA, data.Y, data.P, np.eye(testA.shape[0]))

# coo = H.tocoo()
# SP = spmatrix(coo.data, coo.row.tolist(), coo.col.tolist())
# F = cholmod.symbolic(SP)
# empty = cholmod.numeric(SP,F)
# factor = cholmod.getfactor(F)
# A = sparse.coo_matrix(([i for i in factor],([i for i in factor.I],[i for i in factor.J])))
# print(sparse.lil_matrix(np.linalg.cholesky(H.todense())))
